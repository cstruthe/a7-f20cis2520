#include "ttt.h"
#include "header.h"
void init_boards()
{
    int i = 0;
    //init is all set to zero
    for (i = 0; i < HSIZE; i++)
    {
        htable[i].init = 0;
    }
}

int depth(Board board)
{
    int i = 0;
    int boarddepth = 0;
    char temp;
    //determine depth of the board by checking how many x and o's are on the board
    for (i = 0; i < 9; i++)
    {
        temp = board[pos2idx[i]];
        if (temp == 'X' || temp == 'O')
        {
            boarddepth++;
        }
    }
    return boarddepth;
}
char winner(Board board)
{
    int xcount = 0;
    int ocount = 0;
    char temp = '?';
    int i = 0;
    int j = 0;
    int os[9] = {0};
    int xs[9] = {0};
    int owin = 0;
    int xwin = 0;
    int index = board_hash(board);
    //gets all the values from the board
    for (i = 0; i < 9; i++)
    {
        if (board[pos2idx[i]] == 'X')
        {
            xs[i] = 1;
        }
        else if (board[pos2idx[i]] == 'O')
        {
            os[i] = 1;
        }
    }
    //then goes through the values found and sees if the board matches
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 3; j++)
        {
            if (os[WINS[i][j]] == 1)
            {
                owin++;
            }
            else if (xs[WINS[i][j]] == 1)
            {
                xwin++;
            }
        }
        //if more than three in an array matches, it will return that x or o wins
        if (xwin == 3)
        {
            temp = 'X';
        }
        else if (owin == 3)
        {
            temp = 'O';
        }
        xwin = 0;
        owin = 0;
    }
    //checking if game is over and nobody won
    if (depth(board) == 9 && temp != 'X' && temp != 'O')
    {
        temp = '-';
    }
    else if (depth(board) != 9 && temp != 'X' && temp != 'O')
    {
        //if the game isn't over
        temp = '?';
    }
    return temp;
}
char turn(Board board)
{
    int bdepth = depth(board);
    int modular = bdepth % 2;
    char result;
    char win = winner(board);
    //if modular returns 0 and the depth is less than 0 and not a win
    if (modular == 0 && bdepth != 9 && win == '?')
    {
        result = 'X';
    }
    else if (modular == 1 && bdepth != 9 && win == '?')
    {
        //if the modular returns one and game is not over
        result = 'O';
    }
    else if (bdepth == 9 || win != '?')
    {
        //if game is over
        result = '-';
    }
    return result;
}
void init_board(Board board)
{
    //initiazible that board
    //index
    int boardindex = board_hash(board);
    //init
    htable[boardindex].init = 1;
    //turn
    htable[boardindex].turn = turn(board);
    //depth
    htable[boardindex].depth = depth(board);
    //copoes the board over
    strcpy(htable[boardindex].board, board);
    //finds winner
    htable[boardindex].winner = winner(board);
}
void join_graph(Board board)
{
    int i = 0;
    int boardindex = board_hash(board);
    Board copy;
    char nexturn;
    int nextindex = 0;
    char win = winner(board);
    for (i = 0; i < 9; i++)
    {
        if (board[pos2idx[i]] == 'X' || board[pos2idx[i]] == 'O' || win != '?')
        {
            //if there's a x or o at the position and the game is not over
            htable[boardindex].move[i] = -1;
        }
        else
        {
            //copies the board
            strcpy(copy, board);
            //then you figure out who's turn it is
            nexturn = turn(copy);
            //then you put it in the appriate spot
            copy[pos2idx[i]] = nexturn;
            //then compute the new board's hash value
            nextindex = board_hash(copy);
            //then you put the 
            htable[boardindex].move[i] = nextindex;
            //then you make a new position for the next board
            if (htable[nextindex].init != 0)
            {
                //if the hash hash already been initialized
            }
            else
            {
                //if not it restarts the process
                init_board(copy);
                join_graph(copy);
            }
        }
    }
}
void compute_score()
{
    int i = 0;
    int j = 0;
    int k = 0;
    int best = 0;
    int nextindex = 0;
    Board copy;
    //starts from the dpeth of nine
    for (j = 9; j >= 0; j--)
    {
        for (i = 0; i < HSIZE; i++)
        {
            //if the current depth match and the board exist it will then go into calculations
            if (htable[i].init == 1 && htable[i].depth == j)
            {
                if (htable[i].winner == 'X')
                {
                    //if x is winner
                    htable[i].score = 1;
                }
                else if (htable[i].winner == 'O')
                {
                    //if 0 is winner
                    htable[i].score = -1;
                }
                else if (htable[i].winner == '-')
                {
                    //if no one is winner
                    htable[i].score = 0;
                }
                else if (htable[i].turn == 'X')
                {
                    best = -2;
                    //if game is not over and x's turn goes through the moves
                    for (k = 0; k < 9; k++)
                    {
                        if (htable[i].move[k] != -1)
                        {
                            //it then gets the maximum score of all the move indexes
                            strcpy(copy, htable[i].board);
                            copy[pos2idx[k]] = 'X';
                            nextindex = board_hash(copy);
                            if (best < htable[nextindex].score)
                            {
                                best = htable[nextindex].score;
                            }
                        }
                    }
                    //and then puts it as the score
                    htable[i].score = best;
                }
                else if (htable[i].turn == 'O')
                {
                    //if game is not over and it is o's turn
                    best = 2;
                    for (k = 0; k < 9; k++)
                    {
                        if (htable[i].move[k] != -1)
                        {
                            //it then finds the minimum of all the score values of the move
                            strcpy(copy, htable[i].board);
                            copy[pos2idx[k]] = 'O';
                            nextindex = board_hash(copy);
                            if (best > htable[nextindex].score)
                            {
                                best = htable[nextindex].score;
                            }
                        }
                    }
                    //amd then puts it as the score
                    htable[i].score = best;
                }
            }
        }
    }
}
int best_move(int board)
{
    char turn = htable[board].turn;
    int best = 0;
    int returnindex = 0;
    int i = 0;
    int nextindex = 0;
    Board copy;
    //if it's x's turn
    if (turn == 'X')
    {
        best = -2;
        //finds the move with the highest score so it's the most likely to win
        for (i = 0; i < 9; i++)
        {
            if (htable[board].move[i] != -1)
            {
                strcpy(copy, htable[board].board);
                copy[pos2idx[i]] = 'X';
                nextindex = board_hash(copy);
                if (best < htable[nextindex].score)
                {
                    best = htable[nextindex].score;
                    returnindex = i;
                }
            }
        }
    }
    else if (turn == 'O')
    {
        //if it's O's turn
        best = 2;
        //finds the move with the lowest score so it's the most likely to win
        for (i = 0; i < 9; i++)
        {
            if (htable[board].move[i] != -1)
            {
                strcpy(copy, htable[board].board);
                copy[pos2idx[i]] = 'O';
                nextindex = board_hash(copy);
                if (best > htable[nextindex].score)
                {
                    best = htable[nextindex].score;
                    returnindex = i;
                }
            }
        }
    }
    return returnindex;
}
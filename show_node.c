#include "ttt.h"
#include "header.h"
int main(int argc, char **argv)
{
    int i=0;
    int index=0;
    //makes all the boards
    init_boards();
    init_board(START_BOARD);
    join_graph(START_BOARD);
    //computes score
    compute_score();
    //then prints out all the input from the user
    for(i=1; i<argc; i++){
        index=atoi(argv[i]);
        print_node(htable[index]);
    }
    return 0;
}